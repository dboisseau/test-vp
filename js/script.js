$(function() {
	var filters = [];
	filters['color'] = [];
	filters['price_range'] = [];
	filters['region'] = [];
	var activeFilters = [];
	activeFilters['color'] = [];
	activeFilters['price_range'] = [];
	activeFilters['region'] = [];

	function addToFilter(filterType, value) {
		slug = slugify(value);
		if(filters[filterType].indexOf(slug) === -1) {
			filters[filterType].push(slug);
		}
	}

	function slugify(string) {
		// Trim string
		slugifiedString = string.trim().toLowerCase();

		// Replace accent characters and convert spaces to hyphens
		var search  = 'éèàôû _';
		var replace = 'eeaou--';
		for (var i=0, l=search.length ; i<l ; i++) {
			slugifiedString = slugifiedString.replace(new RegExp(search.charAt(i), 'g'), replace.charAt(i));
		}

		return slugifiedString;
	}

	function activateFilter(filterType, filterValue) {
		activeFilters[filterType].push(filterValue);
	}

	function deactivateFilter(filterType, filterValue) {
		activeFilters[filterType].splice(activeFilters[filterType].indexOf(filterValue), 1);
	}

	function refreshResults() {
		// If no filters, show all
		if(activeFilters['color'].length == 0 &&
		activeFilters['price_range'].length == 0 &&
		activeFilters['region'].length == 0) {
			$('.wine').show();
		}
		else {
			// Hide all results
			// $('.wine').hide();

			// TODO Show results according to selected filters
		}
	}

	// Load json data
	$.getJSON("./fdv2017.json", function(wines) {
		$.each(wines, function(key, wine) {

			// Populate filters with JSON content
			addToFilter('color', wine.color);
			addToFilter('price_range', wine.price_range);
			addToFilter('region', wine.region);

			// CSS classes for the current wine's filters
			var currentFilters = [slugify(wine.color), slugify(wine.price_range), slugify(wine.region)];

			// Append data to HTML
			var html =	'<div class="wine ' + currentFilters.join(' ') + '">' +
							'<div class="wine-description">' +
								'<p class="domain">' + wine.domain + '</p>' +
								'<p class="appelation">' + wine.appellation + '</p>' +
								'<p class="millesime">' + wine.millesime + '</p>' +
								'<div class="price">' + wine.price_vp_bundle.toFixed(2).toString().replace('.', ',') + '&nbsp;&euro;</div>' +
							'</div>' +
							'<div class="wine-img">' +
								'<img src="http://via.placeholder.com/800x500?text=' + wine.domain + '">' +
							'</div>' +
						'</div>';

			$(html).appendTo($('#wines'));
		});

		// Create filters
		$.each(filters.color, function(key, value) {
			$('<li><label><input data-filter-type="color" class="filter" type="checkbox" value="' + value + '" /> ' + value + '</label></li>').appendTo('.filter-color > ul');
		});
		$.each(filters.price_range, function(key, value) {
			$('<li><label><input data-filter-type="price_range" class="filter" type="checkbox" value="' + value + '" /> ' + value + '</label></li>').appendTo('.filter-price_range > ul');
		});
		$.each(filters.region, function(key, value) {
			$('<li><label><input data-filter-type="region" class="filter" type="checkbox" value="' + value + '" /> ' + value + '</label></li>').appendTo('.filter-region > ul');
		});

		// Filters behaviour
		$('.filter').on('click', function() {
			var self = $(this);

			var isChecked = self.is(':checked'),
				filterType = self.data('filterType'),
				filterValue = self.val();
			if(isChecked) {
				activateFilter(filterType, filterValue);
			}
			else {
				deactivateFilter(filterType, filterValue);
			}

			// Refresh results
			refreshResults();

			// Update article count
			refreshArticleCount();
		});
	});

	// Collapsable filters for mobile devices
	$('.title').on('click', function() {
		$(this).parent().toggleClass('mobile-show');
	})
});
