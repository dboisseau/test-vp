"use strict";

var gulp = require('gulp'),
    sass = require('gulp-sass'),
  concat = require('gulp-concat'),
  uglify = require('gulp-uglify'),
  rename = require('gulp-rename');

gulp.task("compileSass", function() {
    return gulp.src('scss/style.scss')
        .pipe(sass())
        .pipe(gulp.dest('./'));
});

gulp.task("concatJs", function() {
    return gulp.src([
        './node_modules/jquery/dist/jquery.min.js',
        './js/script.js'
        ])
    .pipe(concat('concat.js'))
    .pipe(gulp.dest('js'));
});

gulp.task("uglifyScripts", ["concatJs"], function() {
    return gulp.src('js/concat.js')
        .pipe(uglify())
        .pipe(rename('concat.min.js'))
        .pipe(gulp.dest('js'));
});

gulp.task("default", ["compileSass", "uglifyScripts"], function() {

});
